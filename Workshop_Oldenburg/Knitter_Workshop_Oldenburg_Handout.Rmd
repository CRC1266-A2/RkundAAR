---
title: Reproduzierbare Geographie mit R
author: Daniel Knitter
bibliography: Literatur.bib
output:
  html_document:
    toc: true
    toc_float: true
---

# Weiterführende Literatur

Zum *literate programming* siehe @knuth1984. Eine Einführung in R Markdown bietet das Buch von @xienodate. Für mehr Informationen zur Erstellung von Dokumenten, die normalen Text, Code, Ergebnisse und Abbildungen integrieren siehe @xie2014 oder @xie2017.

Eine Einführung in die *reproduzierbare Forschung* und Analyse bietet @gandrud2014. Wenn die Aufgaben/Lösungen entsprechend formuliert sind lassen sich mit R sogar Klassenarbeiten schreiben (und auswerten): http://www.r-exams.org/

Die Programmiersprache `R` mit ihren diversen Möglichkeiten und Anwendungsbereichen wird von sehr vielen Autoren behandelt. Am Populärsten ist derzeit das Buch 
von @grolemundnodate. Für die Geographie am Interessantesten sind meiner Meinung nach (alle mit Code zum Reproduzieren der verwendeten Beispiele): 

- die grundlegenden Methoden: @harris2016
- darauf aufbauend allgemein quantitativ-räumliche Methoden: @bivand2013
- für Punktmusteranalysen: @baddeley2016
- ein Buch, das gerade online live entsteht und schon jetzt tolle Beispiele enthält: @lovelace2018

Und hier noch einige deutschsprachige Titel die in R und statistische Analysen einführen:

- @sachs2006
- @ligges2008
- @wollschlager2012
- @wollschlager2016

Dank der großen Popularität von R ist das Internet voll von Beispielen, Tutorials, Erklärungen...

- Nutzen Sie google. Beispiel: "hurricane data analysis R" oder "zensusdaten analysieren R statistik"
- Folgen und nutzen Sie R-Mailinglisten ([link](https://www.r-project.org/mail.html)); es gibt auch eine eigene Liste direkt für geographische Fragen: R-sig-geo ([link](https://stat.ethz.ch/mailman/listinfo/R-SIG-Geo)).
- Wenn Sie Videos mögen: Youtube...
- Wenn Sie online Kurse (MOOC) mögen: [coursera](https://www.coursera.org/learn/r-programming), [edX](https://www.edx.org/learn/r-programming), [udacity](https://eu.udacity.com/course/data-analysis-with-r--ud651)
- Wenn Sie interaktive online Tutorials mögen: [datacamp](https://www.datacamp.com/courses/free-introduction-to-r); eine nette Zusammenstellung gibt es auch [hier](https://www.rstudio.com/online-learning/)
- und wenn Sie einfach durch die riesige Welt der R Nutzer und Blogger stöbern wollen: [R-Bloggers](https://www.r-bloggers.com/); dort gibt es noch eine große Menge weiterer Links zu hilfreichem Material um R zu lernen.
- R live in ihrem Browser ausprobieren? Geht auch. Zum Beispiel [hier](http://rextester.com/l/r_online_compiler) oder [hier](https://wandbox.org/)

# Software

Quelloffene Programme, die das wissenschaftliche Arbeiten (d.h. hypothesengetrieben und reproduzierbar) vereinfachen und das Erstellen professioneller Dokumente ermöglichen:

- [R](https://www.r-project.org/): Werkzeug zur statistischen Analyse und Darstellung...und dank tausender Erweiterungen de facto *das* Werkzeug für (räumliche) Datenanalysen (Alternative: Python)
- [RStudio](https://www.rstudio.com/): "moderne" Benutzeroberfläche für R und Editor zur Erstellung von "Literate programming" Dokumenten (ergänzt durch die Pakete `rmarkdown` (https://rmarkdown.rstudio.com/) und `knitr` (https://yihui.name/knitr/) sowie den universellen Dokumentkonverter [pandoc](https://pandoc.org/))
- [Zotero](https://www.zotero.org/): Literaturverwaltung mit Browserunterstützung
- [git](https://git-scm.com/): Versionskontrolle

# Quellen frei verfügbarer Daten

## Daten, die direkt mittels R abgerufen werden können: 

### Zensusdaten

- CIA world fact book und mehr ([link](https://cran.r-project.org/web/packages/mosaic/mosaic.pdf))
- EU Zensusdaten ([link](https://cran.r-project.org/web/packages/eurostat/index.html))
- EU Social Survey ([link](https://ropensci.github.io/essurvey/articles/intro_ess.html#analyzing-ess-data); [Beispielanalyse](http://asdfree.com/european-social-survey-ess.html))
- Daten zur menschlichen Populationsdynamik (global; [link](https://github.com/ropensci/rgpdd))

### Geodaten

- OpenStreetMap Daten ([link](https://github.com/ropensci/osmdata))
- Natural Earth Data: Kartenbasisdaten in unterschiedlichen Auflösungen ([link](https://github.com/ropensci/rnaturalearth))
- Administrative Einheiten, global. (verfügbar im Paket `raster` oder [hier](http://gadm.org/))
- Digitales Geländemodell (verfügbar im Paket `raster`)
- Landsat Satellitenbilder herunterladen und auswerten ([link](https://github.com/ropensci/getlandsat))

### Wetter- und Klimadaten

- Tägliche Temperaturdaten ([link](https://github.com/ropensci/GSODR))
- Klimadaten, die von der Weltbank verwendet werden ([link](https://github.com/ropensci/rWBclimate))
- NOAA Daten ([link](https://github.com/ropensci/rnoaa))
- Klimamodell-Ergebnisse (General Circulation Models (GCM); Fokus auf Klimawandel, Landwirtschaft und Ernährungssicherheit (CCAFS); [link](https://github.com/ropensci/ccafs))

## Andere Datenquellen

- Daten zum Temperaturwandel auf der Erde ([link](https://data.giss.nasa.gov/gistemp/); sehr anschauliche, [grafische Umsetzung](https://www.flickr.com/photos/150411108@N06/43350961005/))
  <iframe width="560" height="315" src="https://www.youtube.com/embed/Z4bSxb5THm4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
- Sentinel 1 und 2 Satellitenbilder von der ESA (frei lizenziert) gibt es [hier](https://scihub.copernicus.eu/dhus/#/home)
- [EarthExplorer](https://earthexplorer.usgs.gov/) der NASA (Satellitenbilder, Digitale Geländemodelle von verschiedenen Sensoren wie Landsat, ASTER, SRTM, Corona), kostenlose Registrierung erforderlich
- EUDEM - das digitale Geländemodell der europäischen Union (25m) [link](https://land.copernicus.eu/imagery-in-situ/eu-dem/eu-dem-v1.1/view), kostenlose Registrierung erforderlich
- CORINE Landnutzungsdaten der europäischen Union; drei Zeitscheiben (1990, 2000, 2006) sowie entsprechende "change layer" sind [hier](https://land.copernicus.eu/pan-european/corine-land-cover/view) bereitgestellt; kostenlose Registrierung erforderlich


# Weitere nützliche R Pakete

Weitere nützliche R Pakete, die im Workshop bisher nicht erwähnt wurden (da es weit mehr als 10.000 R Pakete gibt ist diese Liste natürlich unvollständig):

- Thematische Karten erstellen ([link](https://github.com/riatelab/cartography))
- Thematische Karten erstellen 2 ([link](https://github.com/mtennekes/tmap/))
- Walter-Lieth Diagramme und mehr einfach plotten ([link](https://github.com/brry/berryFunctions))


- statische Karten in R erstellen mittels tmap, ggmap, ggplot2 und base R ([link](https://bhaskarvk.github.io/user2017.geodataviz/notebooks/02-Static-Maps.nb.html))
- ggmap cheat sheet ([link](https://www.nceas.ucsb.edu/~frazier/RSpatialGuides/ggmap/ggmapCheatsheet.pdf))

Zu erwähnen ist noch R Shiny womit Sie mit geringem Aufwand interaktive und reaktive (!) Browserprogramme erstellen können. Beispiele gibt es [hier](https://shiny.rstudio.com/gallery/).

# Literatur
